##--------------------------------------------------------------------------------------------------Importation------------------------------------------------------------------------------------------------------------------------------------------##

# Importation des paquets qui vont être utilisés pour le programme #
import easygui as eg
import random as ran
import pygame as py

##-------------------------------------------------------------------------------------------------Initialisation----------------------------------------------------------------------------------------------------------------------------------------##

# Voici une liste de dictionnaires qui sert ici de base de données contenant toutes les questions ainsi que leurs réponses pour le programme qui est un jeux et plus précisément un quiz. Cette liste comprend 25 dictionnaires #
Questionnaire =[{"Quest":"Les éléphants n'ont que quatres dents. Vrai ou faux ?" , "Rep":"vrai"},
                {"Quest":"L'océan Atlantique est le plus grand océan au monde. Vrai ou faux ?" , "Rep":"faux"},
                {"Quest":"Dans quelle ville se trouve la statue de la Liberté ?" , "Rep":"new york"},
                {"Quest":"Quel instrument scientifique utilise-t-on pour regarder des objets très petits ?" , "Rep":"microscope"},
                {"Quest":"Qui a mis au point le vaccin contre la rage ? (Prénom et Nom)" , "Rep":"louis pasteur"},
                {"Quest":"L'île de Pâques appartient au Pérou. Vrai ou faux ?" , "Rep":"faux"},
                {"Quest":"Où se lève le Soleil ?" , "Rep":"est"},
                {"Quest":"Quelle a été l'épreuve d'ouverture des premiers Jeux olympiques féminins, en 1900 : le croquet, la natation ou le tennis ?" , "Rep":"croquet"},
                {"Quest":"Quelle ville a pour signification 'port parfumé' ?" , "Rep":"hong kong"},
                {"Quest":"De quel château Le Nôtre a conçu les jardins ?" , "Rep":"versailles"},
                {"Quest":"En quelle année les Etats-Unis ont acheté la Louisiane à la France pour seulement 15 millions de dollars ?" , "Rep":"1803"},
                {"Quest":"Quel numéro le joueur de basket Michael Jordan portait-il : 16, 23 ou 32 ?" , "Rep":"23"},
                {"Quest":"En quelle année le langage python a-t-il été créé ?" , "Rep":"1991"},
                {"Quest":"Dans quoi Harpagon, l'avare de Molière, range-t-il son argent : une cassette, un coffre ou une malle ?" , "Rep":"cassette"},
                {"Quest":"Qui a composé l'opéra 'Le Mariage de Figaro' ?" , "Rep":"mozart"},
                {"Quest":"Quel pays a une équipe nationale de rugby surnomméé 'les Pumas' ?" , "Rep":"argentine"},
                {"Quest":"Quelle religion a comme livre sacré 'la Torah' ?" , "Rep":"judaïsme"},
                {"Quest":"Quelle est la planète la plus lourde su système solaire ?" , "Rep":"jupiter"},
                {"Quest":"Comment appelle-t-on le fait de renverser 10 quilles du premier coup au bowling ?" , "Rep":"strike"},
                {"Quest":"Qui a signé l'article 'J'accuse' révélant l'affaire Dreyfus en 1898 ? (Prénom et Nom)" , "Rep":"émile zola"},
                {"Quest":"Quel personnage de la mythologie grecque est souvent représenté portant le monde sur ses épaules ?" , "Rep":"atlas"},
                {"Quest":"Combien de membres le Conseil de sécurité des Nations unies compte-t-il : 5, 10 ou 15 ?" , "Rep":"15"},
                {"Quest":"Dans quel sport Tony Hawk, souvent surnommé 'l'homme oiseau', s'illustre-t-il ?" , "Rep":"skateboard"},
                {"Quest":"Quel physicien avait prédit l'existence des trous noirs bien avant qu'on sache les détecter ? (Prénom et Nom)" , "Rep":"albert einstein"},
                {"Quest":"Quel recueil rassemble les histoires racontées par Shéhérazade au sultan ?" , "Rep":"les mille et une nuits"}]

# Initialisation du module mixer du paquet pygame qui permet de jouer des sons cours #
py.mixer.init()

# Les 3 variables suivantes qui sont des listes contenant les images pour le quiz sont définies #
image_quiz = "julienlepers_quiz.jpeg"
image_oui = ["julienlepers_oui_1.gif", "julienlepers_oui_2.jpeg", "julienlepers_oui_3.gif"] 
image_non = ["julienlepers_non_1.png", "julienlepers_non_2.gif", "julienlepers_non_3.jpeg"]

# Les variables suivantes qui sont constantes sont définies. Elles sont dites constantes car elles ne varient pas en fonction des réponses et du score du joueur #
titre = "Questions pour un Champion"
bouton = "Suivant"
bouton_fin = "A plus"
message_presentation = "Salut à toi, jeune curieux et bienvenue dans ce quizzzz : \nQuestions pour un Champion ! \nAujourd'hui, tu vas tester tes connaissances dans de nombreux domaines et prouver à tes amis et au monde que TU ES LE MEILLEUR ! Alors sans plus attendre voici les règles du jeu ..."
message_rules = "Une série de questions va-t-être posée et tu vas devoir écrire la réponse que tu penses être juste : \n1- Une réponse vaut 1 point, une mauvaise ne vaut rien. \n2- La réponse s'écrit directement sans pronom ou déterminant sauf s'il s'agit d'une oeuvre. \n3- Une banque de 25 questions est proposée. Tu peux donc choisir dès maintenant à combien de questions tu veux répondre dans un maximum de 25. \nNote le ici et sur ce, bonne chance !"

##---------------------------------------------------------------------------------------------------------------Entrée---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ##

    # Présentation du jeu #  
Présentation = eg.msgbox(message_presentation, titre, bouton, image_quiz)
    # Annonce des règles + Demande du nombre de questions à poser #
nb = eg.enterbox(msg = message_rules)

##-------------------------------------------------------------------------------------------------------------Traitement-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ##

    # Fonction  qui créer une boucle d'une longueur donnée en fonction du nombre de questions demandées précédemment. A chaque tour, une question est posée et le joueur doit y répondre correctement. A la fin de la boucle, un message de fin apparaît puis le jeu se termine #
def Epreuve(nombredequestions = int(nb)):
    
    # Compteur qui enregistre le score du joueur en fonction de ses bonnes et mauvaises réponses #
    compteur_score = 0
    
    for i in range(nombredequestions):
        
        # Création d'une variable qui prend un des dictionnaires au hasard de la liste du début dénommée "Questionnaire" #
        Question = ran.choice(Questionnaire)
        
        # Ce dictionnaire est ensuite supprimer temporairement de la liste pour que la question ne revienne pas une deuxième fois durant le quiz #
        Questionnaire.remove(Question)
        
        # La question va être affichée à l'aide de l'interface graphique Easygui et avec son numéro d'apparition en fonction de la boucle. (Exemple : 3. Qui est Madame de Lafayette ?, si la question est la 3ème du quiz) #
        # Cette réponse va ensuite être stocker dans la variable "reply" #
        reply = eg.enterbox(msg = str(i+1) + ". " + Question["Quest"]).lower()
        
        # Si la réponse correspond à celle associer dans le dictionnaire, alors un petit bruitage est joué, le compteur augmente de 1 et un message aléatoire de la liste "message_oui" pour féliciter le joueur ainsi que son score et une image choisie aussi aléatoirement dans "image_oui" apparaît dans une messagebox Easygui #
        if reply == Question["Rep"] :
            py.mixer.music.load("/home/pi/Desktop/Projet_info/Questions_pour_un_champion/julien_ohoui.mp3")
            py.mixer.music.play()
            compteur_score += 1
            message_oui = ["Oh oui oui oui oui ouiiiiiiiii !!!!! Bravo champion ! Ton score est maintenant de " + str(compteur_score) + " points !", "Pas mal ! Tu as maintenant " + str(compteur_score) + " points !", "Tu es vraiment impressionnant. Déja " + str(compteur_score) + " points ! Même moi je ne fais pas mieux ..."]
            eg.msgbox(ran.choice(message_oui), titre, bouton, ran.choice(image_oui))
        
        # Si la réponse  ne correspond pas à celle associer dans le dictionnaire, alors un autre petit bruitage est joué, le compteur n'augmente pas et un message aléatoire de la liste "message_non" pour dire au joueur qu'il a eu tort ainsi que la bonne réponse, son score et une image choisie aléatoirement dans "image_non" apparaît dans une messagebox Easygui #
        else:
            py.mixer.music.load("/home/pi/Desktop/Projet_info/Questions_pour_un_champion/julien_ohnon.mp3")
            py.mixer.music.play()
            message_non = ["Ahhh c'est dommage ... La réponse était : " + Question["Rep"] + " ! Ton score est toujours de " + str(compteur_score) + " points !", "C'est non ! Tu pensais vraiment que la réponse était " + reply + " ? C'était bien évidemment : " + Question["Rep"] + ". Ton score est toujours bloqué à " + str(compteur_score) + " points !"]
            eg.msgbox(ran.choice(message_non), titre, bouton, ran.choice(image_non))
    
    # Les deux variables qui suivent sont définies seulement maintenant car elles ne sont pas constantes comme celles au début du programme, c'est à dire qu'elles prennent en compte le compteur final qui varie en fonction du résultat du joueur à chaque quiz #
    # Initialisation du message de fin général #
    message_fin = "Eh oui ! Déja ... Je dois t'annoncer (avec un peu de tristesse quand même :( )"
    # Initialisation de la liste comportant les messages de fin spécifiques : un pour les joueurs ayant eu plus que la moyenne et un pour ceux ayant eu la moyenne ou moins #
    message_fin_spé = [" que cette édition de Questions pour un Champion est finie et que tu as bien réussi dans l'ensemble. Ton score final est de " + str(compteur_score) + " points. C'est vraiment pas mal." , "Même moi je ne fais pas mieux mais, tu peux toujours t'améliorer ! Merci d'avoir participé à mon questionnaire, révise bien et à bientôt j'éspère !" , " que cette édition de Questions pour un Champion est finie et que tu n'as pas vraiment été brillant aujourd'hui. Ton score final est de " + str(compteur_score) + " points." , "Je ne veux pas te décourager mais même mon petit cousin a fait mieux. Mais ne panique pas, ne te décourage pas et retourne réviser. Merci à toi d'avoir participé à mon questionnaire et à bientôt mon champion j'éspère !"]    
    
    # Affichage du message de fin pour les joueurs ayant eu plus que la moyenne #
    if compteur_score > int(nb) // 2:
        eg.msgbox(message_fin + message_fin_spé[0], titre, bouton)
        eg.msgbox(message_fin_spé[1], titre, bouton)
    # Affichage du message de fin pour les joueurs ayant eu la moyenne ou moins #
    else:
        eg.msgbox(message_fin + message_fin_spé[2], titre, bouton)
        eg.msgbox(message_fin_spé[3], titre, bouton_fin)
    
    return None

##---------------------------------------------------------------------------------------------------------------Sortie-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ##

print(Epreuve())

# Cette fonction et pure puisqu'elle renvoie "None" en sortie. Il n'y a pas besoin de renvoyer quelque chose d'autre en sortie car les enterboxs et messageboxs s'affichent directement #

    

