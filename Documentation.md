# Questions pour un champion

Le programme que je vais vous présenter est le fruit d'un travail mélangeant les compétences apprises durant les 6 derniers mois en informatique. Il s'agit d'un quiz non pas de type **QCM** mais où le joueur **rentre lui même** ce qu'il pense être la bonne réponse. J'ai appelé ce jeu __**Questions pour un champion**__ en référence au célèbre jeux télévisé du même nom. Il est d'ailleurs possible que Julien Lepers fasse quelques apparitions ... Ce jeu est jouable une personne à la fois mais il n'est cependant pas interdit de se faire aider par ses amis ; )
Je vais donc vous expliquer tout d'abord comment faire pour télécharger puis exécuter le programme et je vais ensuite vous expliquer son fonctionnement.

## Comment avoir le jeu chez soi ?

Pour pouvoir jouer à **Questions pour un champion** depuis votre ordinateur, rien de plus simple ... Suivez juste les consignes ci-dessous :

 1. Cliquez tout d'abord sur le fichier *Questions_pour_un_champion.py* présent sur le dépôt **Questions pour un champion**.
 2. Cliquez ensuite sur le petit bouton **download** en haut à droite pour télécharger le fichier et enregistrez le ensuite où vous le souhaitez.
 3. Pour que le programme fonctionne, les paquets **easygui**, **random** et **pygame** doivent être installés. Pour cela vous devez rentrer dans votre terminal : pip3 install nom_du_paquet.  *Exemple :* pip3 install easygui. Sous Linux, c'est simple mais si vous n'avez pas de terminal sous **Windows**, cliquez [ici](https://www.microsoft.com/fr-fr/p/windows-terminal-preview/9n0dx20hk701?activetab=pivot:overviewtab) et sur **macOS**, [ici](https://fr.wikihow.com/ouvrir-le-Terminal-sur-un-Mac).
 4. Téléchargez ensuite les images et les sons présents dans le dossier du même nom *images et sons* puis enregistrez les au même endroit que celui où vous avez enregistré le programme.
 5. Si vous avez déjà un **interpréteur python**, passez à l'étape 6, sinon restez ici.  Pour pouvoir exécuter le fichier que vous venez de télécharger, vous avez besoin d'un **interpréteur python** car ce fichier est au format .py. Vous pouvez donc choisir parmi une large gamme d’interpréteurs mais si vous ne savez pas lequel choisir, je vous conseille *Visual Studio Code* ou *VSC*. Pour l'installer, rien de plus simple ... Cliquez [ici](https://code.visualstudio.com/Download) et choisissez ensuite le bon fichier à télécharger en fonction de votre système d'exploitation **Windows**,  **macOS** ou **Linux**.
 6. Ouvrez ensuite cet interpréteur et faites **upload** le fichier *Questions_pour_un_champion.py* . Pour cela, aller dans *File* puis *Open File* et enfin sélectionner le fichier.
 7. Une fois que le fichier est affiché, vous avez réussi et vous pouvez l'exécuter à votre guise (et même le modifier si vous le souhaitez).

## Comment le jeu fonctionne-t-il ?

**Questions pour un champion** est très facile à comprendre et à prendre en main. Il s'agit tout simplement d'un quiz de culture générale où des questions vous sont posées. Pour y répondre, vous devez simplement rentrer la réponse que vous pensez juste en la tapant à l'aide de votre clavier ! Mais comment s'articule exactement le jeu ? Je vais vous l'expliquer plus en détail ...

 - La partie commence juste après que vous ayez lancé le programme. Un premier message de bienvenue va être afficher et vous proposer de continuer.
 - Un deuxième message va ensuite apparaître et vous expliquer les principales règles. Par précaution, je vais quand même vous les réexpliquer ici :  
 1. Chaque bonne réponse donne 1 point mais une mauvaise n'en donne pas.
 2. Votre réponse doit s'écrire sans pronom ou déterminant. *Exemple :* Quelle pays à pour capitale Paris ? **France**. Sauf si la réponse est une oeuvre.  *Exemple :* Quelle oeuvre de Madame de Lafayette raconte l'histoire d'une jeune princesse vivant à la Cour des Valois? **La princesse de Clèves**.

    Rien de plus simple.

 - A la fin des règles, il vous est proposé de choisir le nombre de questions auquel vous souhaitez répondre dans un maximum de 25 questions car la banque de questions possède au total 25 questions ainsi que la réponse associée à celle ci.
 - Les questions s'affichent ensuite et vous n'avez plus qu'à y répondre ! Si vous ne savez pas, essayer quand même quelque chose ou faites tout simplement *OK* sans n'avoir rien écrit ...
 - Si vous répondez correctement, un message de félicitation s'affiche avec une musique et une image mais si vous répondez mal, alors un message vous donnant la bonne réponse s'affiche avec une autre image et une autre musique. De plus, vous pouvez voir après chaque question votre score. Celui s'affiche en même temps que le message.
 - A la fin du quiz, après avoir répondu à toute les questions posées, un message de fin vous annonce votre score final et commente votre performance. J’espère pour vous que celui-ci sera à la hauteur de vos attentes ! Et si vous voulez rejouer, vous n'avez qu'à relancer le programme.
 -  **Petites astuces :** Vous pouvez valider votre réponse en appuyant directement sur la touche *Enter* ou *Entrer* et pour passer rapidement un message, appuyer sur la touce *Space* ou *Espace*. Enfin si vous souhaitez quitter le jeu sans avoir fini votre partie, cliquez sur le bouton *Cancel*.

## Quelques pistes d'améliorations pour le futur ...

Le programme présenté n'est qu'une première version et peut certainement être encore amélioré. Je vais donc continuer à le perfectionner pour le rendre toujours plus abouti et intéressant pour le joueur mais aussi pour les personnes voulant le modifier. Parmi les **idées**, je pense déjà à :

 - Transformer la banque de questions qui est une liste de dictionnaires en un fichier csv. Il faudrait donc utiliser une fonction qui convertit le fichier csv en liste de dictionnaires dans le programme.
 - Proposer au joueur à la fin de chaque partie la possibilité de rajouter des questions à la banque de questions déjà existante.
 - Mettre en place un timer qui donne au joueur un temps de réponse limité pour augmenter la difficulté.
 - Proposer un mode pour 2 joueurs.
 -  ...
 
 N'hésitez surtout pas à partager vos idées car tout le monde peut participer. Mais surtout, **amusez vous bien et apprenez bien !**

*Alexis LUCAS*
